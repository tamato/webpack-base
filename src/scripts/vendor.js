// has no module
// wtf: module anyway in Window object
var Waypoint = require('waypoints/lib/noframework.waypoints.min');

// Has module
var $ = require('jquery');
var tooltipster = require('tooltipster');

require('tooltipster/dist/css/tooltipster.bundle.css');

$(function() {
	var waypointElement = $('.js-waypoint').get(0);
	var $tooltipElement = $('.js-tooltip');

	$tooltipElement.tooltipster({
		trigger: 'click'
	});

	new Waypoint({
		element: waypointElement,
		handler: function() {
			console.log('Waypoint here')
		}
	})
})