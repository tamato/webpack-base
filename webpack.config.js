var ExtractTextPlugin = require('extract-text-webpack-plugin');
var UglifyJsPlugin = require('uglifyjs-webpack-plugin');
var webpack = require('webpack'); 
var autoprefixer = require('autoprefixer');

const NODE_ENV = process.env.NODE_ENV || 'production';
const IS_DEV = NODE_ENV == 'development';

const plugins = [
	new ExtractTextPlugin('[name].min.css') 
]

if (!IS_DEV) {
	plugins.push(new UglifyJsPlugin({ sourceMap: false }))
}

module.exports = {
	context: __dirname + '/src',
	
	entry: {
		main: './scripts/app.js',
		vendor: './scripts/vendor.js'
	},

	watch: IS_DEV,
	devtool: IS_DEV ? "source-map" : false,
	
	output: {
		path: __dirname + '/dest',
		filename: '[name].js'
	},

	module: {
		loaders: [
			{
	      test: /\.js$/,
	      exclude: /(node_modules|bower_components)/,
	      use: {
	        loader: 'babel-loader',
	        options: {
	          presets: ['env']
	        }
	      }
	    },
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					use: [
          	{ 
          		loader: 'css-loader',  
          		options: { sourceMap: IS_DEV, minimize: !IS_DEV } 
          	}, 
					]
				})
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
          use: [
          	{ loader: 'css-loader',  options: { sourceMap: IS_DEV, minimize: !IS_DEV } }, 
          	{ 
          		loader: 'postcss-loader', 
          		options: {
          			sourceMap: IS_DEV,
          			plugins: [
          				autoprefixer({ browsers: ['last 100 versions'] })
          			]
          		} 
          	},
          	{ loader: 'sass-loader', options: { sourceMap: IS_DEV } }
          ]
				})
			},
			{
				test: require.resolve('waypoints/lib/noframework.waypoints.min.js'),
	      use: 'exports-loader?Waypoint'
			}
		],
	},

	plugins: plugins,

	watchOptions: {
		aggregateTimeout: 300
	},

	resolve: {
		modules: ['node_modules']
	},

	// resolveLoader: {
	// 	modulesDirectories: 'node_modules',
	// 	moduleTemplates: ['*-loader', '*'],
	// 	extensions: ['', '.js']
	// },

};